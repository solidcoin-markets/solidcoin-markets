#!/usr/bin/python
import datetime, json, locale, os, random, socket, threading, time

locale.setlocale(locale.LC_ALL, 'en_US')

class message:
    def __str__(self):
        return "<message "+str(self.prefix)+" "+str(self.command)+" "+str(self.params)+" "+str(self.trailing)+">"

class irc_stream:
    def __init__(self,s):
        self.socket = s
        self.buffer = ""
    def read_messages(self):
        buffer = self.socket.recv(8192)
        messages = []
        if buffer == '':
            #raise socket.error
            m = message()
            m.command = 'DISCONNECTED'
            messages.append(m)
            return messages
        self.buffer += buffer
        while "\r\n" in self.buffer:
            i = self.buffer.find("\r\n")
            line = self.buffer[:i]
            self.buffer = self.buffer[i+2:]
            m = message()
            m.line = line
            if line[0] == ":":
                i = line.find(" ")
                m.prefix = line[1:i]
                line = line[i+1:]
            else:
                m.prefix = None
            i = line.find(" ")
            m.command = line[:i]
            line = line[i+1:]
            m.params = []
            m.trailing = None
            while True:
                if line[0] == ":":
                    m.trailing = line[1:]
                    break
                else:
                    i = line.find(" ")
                    if i == -1:
                        m.params.append(line)
                        break
                    else:
                        m.params.append(line[:i])
                        line = line[i+1:]
            messages.append(m)
        return messages

class solidcoinmarkets_bot(threading.Thread):
    def __init__(self):
        super(solidcoinmarkets_bot, self).__init__()
        self.socket = socket.socket()
        self.socket.settimeout(3)
        self.send_queue = []
    def clear_queue(self):
        for line in self.send_queue:
            try:
                self.socket.sendall(line+'\r\n')
            except socket.error:
                self.active = False
                time.sleep(10)
                self.reconnect()
                return
            time.sleep(1)
        self.send_queue = []
    def connect(self):
        self.socket.connect((self.host,self.port))
        self.reader = irc_stream(self.socket)
        self.send_queue.append("USER slcm 0 * :slcm")
        self.send_queue.append("NICK "+self.random_string())
        #self.send_queue.append("PRIVMSG nickserv :IDENTIFY "+freenodepw)
        self.clear_queue()
        while True:
            scanForNewTrades()
            try:
                for m in self.reader.read_messages():
                    #http://www.networksorcery.com/enp/protocol/irc.htm
                    if m.command == "001": pass #RPL_WELCOME
                    elif m.command == "002": pass #RPL_YOURHOST
                    elif m.command == "003": pass #RPL_CREATED
                    elif m.command == "004": pass #RPL_MYINFO
                    elif m.command == "005": pass #RPL_BOUNCE
                    elif m.command == "250": pass
                    elif m.command == "251": pass #RPL_LUSERCLIENT
                    elif m.command == "252": pass #RPL_LUSEROP
                    elif m.command == "253": pass #RPL_LUSERUNKNOWN
                    elif m.command == "254": pass #RPL_LUSERCHANNELS
                    elif m.command == "255": pass #RPL_LUSERME
                    elif m.command == "265": pass
                    elif m.command == "266": pass
                    elif m.command == "353": #RPL_NAMREPLY
                        self.active = True
                    elif m.command == "372": pass #RPL_MOTD
                    elif m.command == "375": pass #RPL_MOTDSTART
                    elif m.command == "376": pass #RPL_ENDOFMOTD
                    elif m.command == "433": #ERR_NICKNAMEINUSE
                        self.send_queue.append("PRIVMSG nickserv :GHOST "+self.nick+" "+freenodepw)
                        self.send_queue.append('NICK '+self.nick)
                        time.sleep(1)
                    elif m.command == "DISCONNECTED":
                        self.connect()
                    elif m.command == "MODE":
                        self.send_queue.append('NICK '+self.nick)
                    elif m.command == "NICK":
                        if m.trailing == self.nick:
                            self.send_queue.append("JOIN "+self.channel)
                    elif m.command == "PING":
                        self.send_queue.append('PONG '+m.trailing)
                    else: pass
            except socket.timeout:
                pass

            if self.active == True:
                global tradeFiles
                newfile = self.random_string(32)
                if len(tradeFiles) > 0:
                    try: file = tradeFiles.pop(0)
                    except IndexError: continue
                    if os.path.exists('data/irc/'+file):
                        try: os.rename('data/irc/'+file, 'data/irc/'+newfile)
                        except OSError: continue
                        try: json_data = json.load(open('data/irc/'+newfile))
                        except IOError: continue
                        try: os.unlink('data/irc/'+newfile)
                        except OSError: continue
                        strTime = datetime.datetime.utcfromtimestamp(float(json_data['date'])).strftime('%b%d %H:%M:%S')
                        strMarket = json_data['market'].ljust(20)
                        strQuantity = locale.format("%.*f", (4, float(json_data['quantity'])), True).rjust(10)
                        strPrice = formatPrice(locale.format("%.*f", (5, float(json_data['price'])), True))
                        strCurrency = json_data['currency']
                        strLine = strTime + ' \x02' + strMarket + '\x02 ' + strQuantity + ' @ \x02' + strPrice + '\x02  ' + strCurrency
                        self.send_queue.append("PRIVMSG "+self.channel+" :"+unicode(strLine))

            self.clear_queue()
    def random_string(self,length=12):
        return ''.join([random.choice("abcdefghijklmnopqrstuvwxyz") for x in xrange(length)])
    def reconnect(self):
        self.socket = socket.socket()
        self.socket.settimeout(10)
        self.send_queue = []
        self.connect()
    def run(self):
        self.connect()

def formatPrice(num):
    # Minimum two decimal places
    num = num.rstrip("0")
    if num.find(".") + 3 > len(num): num = num + "0" * (num.find(".") + 3 - len(num))
    return " " * (8 - num.find(".")) + num + " " * (6 - (len(num) - num.find(".")))

def scanForNewTrades():
    global tradeFiles
    for file in os.listdir('data/irc/'):
        if os.path.exists('data/irc/'+file) and file not in tradeFiles:
            tradeFiles.append(file)
            tradeFiles.sort()

if os.path.exists('data/') == False: os.mkdir('data/')
if os.path.exists('data/irc/') == False: os.mkdir('data/irc/')

nicks=['slcm0','slcm1','slcm2','slcm3','slcm4','slcm5','slcm6','slcm7','slcm8','slcm9']
freenodepw = open('data/freenode').read().strip()

bots = set()

tradeFiles = []

for nick in nicks:
    bot = solidcoinmarkets_bot()
    bot.host = "irc.freenode.net"
    bot.port = 6667
    bot.nick = nick
    bot.channel = '#solidcoin-markets'
    bot.active = False
    bot.daemon = True
    bot.start()
    bots.add(bot)
    time.sleep(10)

while True: pass