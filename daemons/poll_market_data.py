#!/usr/bin/python
import decimal, httplib, json, logging, os, psycopg2, re, socket, ssl, string, tempfile, threading, time

# Levels: DEBUG, INFO, WARNING, ERROR, CRITICAL
logging.basicConfig(level='INFO');log = logging.getLogger(":")

headers = {'User-Agent': 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/2008121819 Gentoo Firefox/3.0.4'}
pushToIRC = True

def getJSON(url):
    if url.group(1) == "http": connection = httplib.HTTPConnection(url.group(2), timeout=10)
    elif url.group(1) == "https": connection = httplib.HTTPSConnection(url.group(2), timeout=10)

    try: connection.request("GET", url.group(3), '', headers);response = connection.getresponse()
    except socket.error, e: log.error("socket error:"+url.group()+" "+str(e));return
    except socket.timeout, e: log.error("socket timeout:"+url.group()+" "+e.message);return
    except ssl.SSLError, e: log.error("ssl error:"+url.group()+" "+e.message);return

    try: return json.load(response, parse_float=decimal.Decimal)
    except Exception, e: log.error("exception:"+url.group()+" "+e.message+" "+str(response));return
    except ssl.SSLError, e: log.error("ssl error:"+url.group()+" "+e.message);return

def writeJSONToFile(json_data, file):
    f = tempfile.NamedTemporaryFile(delete=False, dir='data/tmp/')
    f.write(json.dumps(json_data))
    f.close()
    os.rename(f.name, file)

if os.path.exists('data/') == False: os.mkdir('data/')
if pushToIRC == True:
    import random, string
    if os.path.exists('data/irc/') == False: os.mkdir('data/irc/')
if os.path.exists('data/tmp/') == False: os.mkdir('data/tmp/')
if os.path.exists('htdocs/') == False: os.mkdir('htdocs/')
if os.path.exists('htdocs/data/') == False: os.mkdir('htdocs/data/')
if os.path.exists('htdocs/data/orderbooks/') == False: os.mkdir('htdocs/data/orderbooks/')

class initMarketWorker(threading.Thread):
    def __init__(self, exchange):
        self.exchange = exchange
        threading.Thread.__init__(self)

    def run(self):
        url_depth = re.search('(https?)://([^/]*)(.*)', self.exchange[2])
        url_ticker = re.search('(https?)://([^/]*)(.*)', self.exchange[1])
        url_trades = re.search('(https?)://([^/]*)(.*)', self.exchange[3])
        #url_trades_canceled = re.search('(https?)://([^/]*)(.*)', self.exchange[4])
        if url_depth is not None or url_ticker is not None or url_trades is not None:# or url_trades_canceled is not None:
            while True:
                if url_depth is not None:
                    self.queryDepth = False
                    if hasattr(self, 'lastQueryDepth') == False: self.lastQueryDepth = 0
                    if self.lastQueryDepth + 30 < time.time(): self.queryDepth = True
                    if self.queryDepth == True:
                        self.getMarketDepth(self.exchange, url_depth)
                        self.lastQueryDepth = time.time()
                if url_ticker is not None:
                    self.queryTicker = False
                    if hasattr(self, 'lastQueryTicker') == False: self.lastQueryTicker = 0
                    if self.lastQueryTicker + 30 < time.time(): self.queryTicker = True
                    if self.queryTicker == True:
                        self.getMarketTicker(self.exchange, url_ticker)
                        self.lastQueryTicker = time.time()
                if url_trades is not None:
                    self.queryTrades = False
                    if hasattr(self, 'lastQueryTrades') == False: self.lastQueryTrades = 0
                    if (string.find(url_trades.group(), "%LASTTRADEID%") != -1 or string.find(url_trades.group(), "%LASTTRADETS%") != -1) and self.lastQueryTrades + 1 < time.time(): self.queryTrades = True
                    if self.lastQueryTrades + 30 < time.time(): self.queryTrades = True
                    if self.queryTrades == True:
                        self.getMarketTrades(self.exchange, url_trades)
                        self.lastQueryTrades = time.time()
                #if url_trades_canceled is not None:
                #    self.getMarketTradesCanceled(self.exchange, url_trades_canceled)
                time.sleep(0.0001)

    def addMarketDepthData(self, result, url_depth, orderdepth):
        for ask in orderdepth['asks']:
            if 'low' not in orderdepth['ticker']: orderdepth['ticker']['low'] = ask[0]
            elif ask[0] < orderdepth['ticker']['low']: orderdepth['ticker']['low'] = ask[0]
        for bid in orderdepth['bids']:
            if 'high' not in orderdepth['ticker']: orderdepth['ticker']['high'] = bid[0]
            elif bid[0] > orderdepth['ticker']['high']: orderdepth['ticker']['high'] = bid[0]
        if 'high' not in orderdepth['ticker']: orderdepth['ticker']['high'] = None
        if 'low' not in orderdepth['ticker']: orderdepth['ticker']['low'] = None
        if orderdepth['ticker']['high'] is None or orderdepth['ticker']['low'] is None:
            orderdepth['ticker']['avg'] = None
        else:
            orderdepth['ticker']['avg'] = str((decimal.Decimal(orderdepth['ticker']['high']) + decimal.Decimal(orderdepth['ticker']['low'])) / 2)
        if orderdepth['ticker']['high'] is not None and orderdepth['ticker']['low'] is not None:
            writeJSONToFile(orderdepth, 'htdocs/data/orderbooks/'+str(result[0]));
            log.info(str(int(time.time()))+":DEPTH:"+url_depth.group(2)+":"+result[5]+" high:"+str(orderdepth['ticker']['high'])+" low:"+str(orderdepth['ticker']['low']))
            db = psycopg2.connect(database="solidcoinmarkets",user="solidcoinmarkets")
            c = db.cursor()
            c.execute("""INSERT INTO depth (
                    depth_market_id, depth_highbid, depth_lowask
                ) VALUES(%s,%s,%s)""", (
                    result[0], orderdepth['ticker']['high'], orderdepth['ticker']['low']
                )
            )
            db.commit()
            c.close()

    def addMarketTickerData(self, result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap):
        if isinstance(ticker_high, bool): ticker_high = None
        if isinstance(ticker_low, bool): ticker_low = None
        if isinstance(ticker_avg, bool): ticker_avg = None
        if isinstance(ticker_vwap, bool): ticker_vwap = None
        if isinstance(ticker_vol, bool): ticker_vol = None
        if isinstance(ticker_last, bool): ticker_last = None
        if isinstance(ticker_buy, bool): ticker_buy = None
        if isinstance(ticker_sell, bool): ticker_sell = None
        log.info(str(int(time.time()))+":TICKER:"+url_ticker.group(2)+":"+result[5]+" high:"+str(ticker_high)+" low:"+str(ticker_low)+" avg:"+str(ticker_avg)+" vwap:"+str(ticker_vwap)+" vol:"+str(ticker_vol)+" last:"+str(ticker_last)+" buy:"+str(ticker_buy)+" sell:"+str(ticker_sell))
        db = psycopg2.connect(database="solidcoinmarkets",user="solidcoinmarkets")
        c = db.cursor()
        c.execute("""INSERT INTO tickers (
                ticker_market_id, ticker_high, ticker_low, ticker_avg, ticker_vwap, ticker_vol, ticker_last, ticker_buy, ticker_sell
            ) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s)""", (
                result[0], ticker_high, ticker_low, ticker_avg, ticker_vwap, ticker_vol, ticker_last, ticker_buy, ticker_sell
            )
        )
        db.commit()
        c.close()

    def addMarketTradesData(self, result, url_trades, tradehistory):
        tradehistory['trades'].sort(key=lambda x: x['date'])
        for trade in tradehistory['trades']:
            db = psycopg2.connect(database="solidcoinmarkets",user="solidcoinmarkets")
            c = db.cursor()
            c.execute("""SELECT trade_market_id, trade_id FROM trades WHERE trade_market_id=%s AND trade_id=%s""", (result[0], trade['tradeid']))
            if c.fetchall() == []:
                log.info(str(int(time.time()))+":TRADE:"+url_trades.group(2)+":"+result[5]+" tid:"+str(trade['tradeid'])+" date:"+str(trade['date'])+" price:"+str(trade['price'])+" quantity:"+str(trade['quantity']))
                c.execute("""INSERT INTO trades (
                        trade_market_id, trade_id, trade_date, trade_price, trade_quantity
                    ) VALUES(%s,%s,to_timestamp(%s),%s,%s)""", (
                        result[0], trade['tradeid'], trade['date'], trade['price'], trade['quantity']
                    )
                )
                db.commit()
                if pushToIRC == True:
                    trade['currency'] = result[5]
                    trade['market'] = result[6]
                    trade['price'] = str(trade['price'])
                    trade['quantity'] = str(trade['quantity'])
                    writeJSONToFile(trade, 'data/irc/'+str(trade['date'])+'_'+''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32)))
            c.close()

    def initMarketDepthVars(self, url_depth):
        json_depth = getJSON(url_depth)
        if json_depth is None or json_depth == {}: return (None, None)
        orderdepth = {u'asks':[],u'bids':[],u'ticker':{}}
        return (json_depth, orderdepth)

    def initMarketTickerVars(self, url_ticker):
        json_ticker = getJSON(url_ticker)
        if json_ticker is None or json_ticker == {}: return
        return json_ticker

    def initMarketTradesVars(self, result, url_trades):
        if string.find(url_trades.group(), "%LASTTRADEID%") != -1:
            db = psycopg2.connect(database="solidcoinmarkets",user="solidcoinmarkets")
            c = db.cursor()
            c.execute("""SELECT trade_id FROM trades WHERE trade_market_id = %s ORDER BY trade_date DESC LIMIT 1""", (result[0],))
            r = c.fetchone()
            c.close()
            if r is None: tradeid = 0
            else: tradeid = r[0]
            url_trades = string.replace(url_trades.group(), "%LASTTRADEID%", str(tradeid))
            url_trades = re.search('(https?)://([^/]*)(.*)', url_trades)
        elif string.find(url_trades.group(), "%LASTTRADETS%") != -1:
            db = psycopg2.connect(database="solidcoinmarkets",user="solidcoinmarkets")
            c = db.cursor()
            c.execute("""SELECT EXTRACT(EPOCH FROM trade_date) FROM trades WHERE trade_market_id = %s ORDER BY trade_date DESC LIMIT 1""", (result[0],))
            r = c.fetchone()
            c.close()
            if r is None: tradets = 0
            else: tradets = r[0]
            url_trades = string.replace(url_trades.group(), "%LASTTRADETS%", str(int(tradets)))
            url_trades = re.search('(https?)://([^/]*)(.*)', url_trades)
        json_trades = getJSON(url_trades)
        if json_trades is None or json_trades == {}: return (None, None)
        tradehistory = {u'trades':[]}
        return (json_trades, tradehistory)

class solidcoin24(initMarketWorker):
    def getMarketDepth(self, result, url_depth):
        json_depth, orderdepth = self.initMarketDepthVars(url_depth)
        if json_depth is None: return
        for ask in json_depth['asks']: orderdepth['asks'].append([str(ask['rate']),str(ask['amount'])])
        for bid in json_depth['bids']: orderdepth['bids'].append([str(bid['rate']),str(bid['amount'])])
        self.addMarketDepthData(result, url_depth, orderdepth)
    def getMarketTicker(self, result, url_ticker):
        json_ticker = self.initMarketTickerVars(url_ticker)
        if json_ticker is None: return
        ticker_high = json_ticker['high']
        ticker_low = json_ticker['low']
        ticker_avg = None
        ticker_vwap = None
        ticker_vol = json_ticker['vol']
        ticker_last = json_ticker['last']
        ticker_buy = json_ticker['bid']
        ticker_sell = json_ticker['ask']
        self.addMarketTickerData(result, url_ticker, ticker_avg, ticker_buy, ticker_high, ticker_last, ticker_low, ticker_sell, ticker_vol, ticker_vwap)
    def getMarketTrades(self, result, url_trades):
        json_trades, tradehistory = self.initMarketTradesVars(result, url_trades)
        if json_trades is None: return
        for trade in json_trades: tradehistory['trades'].append({"date":trade['time'],"tradeid":trade['id'],"price":trade['rate'],"quantity":trade['amount']})
        self.addMarketTradesData(result, url_trades, tradehistory)

marketworkers = []
db = psycopg2.connect(database="solidcoinmarkets",user="solidcoinmarkets")
c = db.cursor()
c.execute("""SELECT
        market_id,
        market_data_ticker,
        market_data_depth,
        market_data_trades,
        market_data_trades_canceled,
        currency_abbreviation,
        exchange_devname
    FROM markets,exchanges,currencies
    WHERE markets.market_currency_id=currency_id
    AND markets.market_exchange_id=exchanges.exchange_id
    AND markets.market_active=TRUE
    """)
for result in c.fetchall():
    w = {
        'solidcoin24':solidcoin24
    }[result[6]](result)
    w.daemon = True
    w.start()
    marketworkers.append(w)
c.close()

#raw_input()
while True:
    time.sleep(1)