--
-- Name: market_type; Type: TYPE; Schema: public; Owner: solidcoinmarkets
--

CREATE TYPE market_type AS ENUM (
    'escrow',
    'exchange'
);


ALTER TYPE public.market_type OWNER TO solidcoinmarkets;

--
-- Name: currencies_currency_id_sequence; Type: SEQUENCE; Schema: public; Owner: solidcoinmarkets
--

CREATE SEQUENCE currencies_currency_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.currencies_currency_id_sequence OWNER TO solidcoinmarkets;

--
-- Name: currencies_currency_id_sequence; Type: SEQUENCE SET; Schema: public; Owner: solidcoinmarkets
--

SELECT pg_catalog.setval('currencies_currency_id_sequence', 143, true);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: currencies; Type: TABLE; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE TABLE currencies (
    currency_id smallint DEFAULT nextval('currencies_currency_id_sequence'::regclass) NOT NULL,
    currency_abbreviation character varying(45) NOT NULL,
    currency_name character varying(45) NOT NULL
);


ALTER TABLE public.currencies OWNER TO solidcoinmarkets;

--
-- Name: depth; Type: TABLE; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE TABLE depth (
    depth_id bigint NOT NULL,
    depth_market_id smallint NOT NULL,
    depth_highbid numeric(32,12),
    depth_lowask numeric(32,12),
    depth_timestamp timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.depth OWNER TO solidcoinmarkets;

--
-- Name: depth_depth_id_seq; Type: SEQUENCE; Schema: public; Owner: solidcoinmarkets
--

CREATE SEQUENCE depth_depth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.depth_depth_id_seq OWNER TO solidcoinmarkets;

--
-- Name: depth_depth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: solidcoinmarkets
--

ALTER SEQUENCE depth_depth_id_seq OWNED BY depth.depth_id;


--
-- Name: exchanges_exchange_id_sequence; Type: SEQUENCE; Schema: public; Owner: solidcoinmarkets
--

CREATE SEQUENCE exchanges_exchange_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.exchanges_exchange_id_sequence OWNER TO solidcoinmarkets;

--
-- Name: exchanges_exchange_id_sequence; Type: SEQUENCE SET; Schema: public; Owner: solidcoinmarkets
--

SELECT pg_catalog.setval('exchanges_exchange_id_sequence', 1, true);


--
-- Name: exchanges; Type: TABLE; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE TABLE exchanges (
    exchange_id smallint DEFAULT nextval('exchanges_exchange_id_sequence'::regclass) NOT NULL,
    exchange_name character varying(100),
    exchange_website character varying NOT NULL,
    exchange_email character varying NOT NULL,
    exchange_freenode character varying(49),
    exchange_phone character varying(20),
    exchange_address character varying,
    exchange_devname character varying(20)
);


ALTER TABLE public.exchanges OWNER TO solidcoinmarkets;

--
-- Name: COLUMN exchanges.exchange_freenode; Type: COMMENT; Schema: public; Owner: solidcoinmarkets
--

COMMENT ON COLUMN exchanges.exchange_freenode IS 'Freenode channel length restricted to 49 characters';


--
-- Name: markets_market_id_sequence; Type: SEQUENCE; Schema: public; Owner: solidcoinmarkets
--

CREATE SEQUENCE markets_market_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.markets_market_id_sequence OWNER TO solidcoinmarkets;

--
-- Name: markets_market_id_sequence; Type: SEQUENCE SET; Schema: public; Owner: solidcoinmarkets
--

SELECT pg_catalog.setval('markets_market_id_sequence', 4, true);


--
-- Name: markets; Type: TABLE; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE TABLE markets (
    market_id smallint DEFAULT nextval('markets_market_id_sequence'::regclass) NOT NULL,
    market_symbol character varying(16) NOT NULL,
    market_website character varying NOT NULL,
    market_data_ticker character varying(128),
    market_data_depth character varying(128),
    market_data_trades character varying(128),
    market_currency_id smallint NOT NULL,
    market_data_trades_all character varying(128),
    market_data_depth_all character varying(128),
    market_active boolean DEFAULT true NOT NULL,
    market_exchange_id smallint,
    market_transactionfee character varying,
    market_type market_type DEFAULT 'exchange'::market_type NOT NULL,
    market_data_trades_canceled character varying(128)
);


ALTER TABLE public.markets OWNER TO solidcoinmarkets;

--
-- Name: settings; Type: TABLE; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE TABLE settings (
    settings_var character varying NOT NULL,
    settings_val character varying NOT NULL
);


ALTER TABLE public.settings OWNER TO solidcoinmarkets;

--
-- Name: tickers; Type: TABLE; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE TABLE tickers (
    ticker_market_id smallint,
    ticker_timestamp timestamp with time zone DEFAULT now(),
    ticker_high numeric,
    ticker_low numeric,
    ticker_avg numeric,
    ticker_vwap numeric,
    ticker_vol numeric,
    ticker_last numeric,
    ticker_buy numeric,
    ticker_sell numeric
);


ALTER TABLE public.tickers OWNER TO solidcoinmarkets;

--
-- Name: trade_volumes; Type: TABLE; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE TABLE trade_volumes (
    trade_volume_market_id smallint NOT NULL,
    trade_volume_day timestamp with time zone NOT NULL,
    trade_volume_daily_volume numeric(32,12) NOT NULL,
    trade_volume_daily_spent numeric(32,12) NOT NULL
);


ALTER TABLE public.trade_volumes OWNER TO solidcoinmarkets;

--
-- Name: trades; Type: TABLE; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE TABLE trades (
    trade_market_id smallint,
    trade_id bigint,
    trade_date timestamp with time zone NOT NULL,
    trade_price numeric(32,12) NOT NULL,
    trade_quantity numeric(32,12) NOT NULL,
    trade_saved timestamp with time zone DEFAULT now() NOT NULL,
    trade_canceled boolean DEFAULT false NOT NULL
);


ALTER TABLE public.trades OWNER TO solidcoinmarkets;

--
-- Name: depth_id; Type: DEFAULT; Schema: public; Owner: solidcoinmarkets
--

ALTER TABLE depth ALTER COLUMN depth_id SET DEFAULT nextval('depth_depth_id_seq'::regclass);


--
-- Data for Name: currencies; Type: TABLE DATA; Schema: public; Owner: solidcoinmarkets
--

INSERT INTO currencies VALUES (1, 'BTC', 'Bitcoin');
INSERT INTO currencies VALUES (2, 'ADF', 'Andorran franc');
INSERT INTO currencies VALUES (3, 'AFN', 'Afghani');
INSERT INTO currencies VALUES (4, 'ALL', 'Lek');
INSERT INTO currencies VALUES (5, 'AMD', 'Dram');
INSERT INTO currencies VALUES (6, 'AOA', 'Kwanza');
INSERT INTO currencies VALUES (7, 'ARS', 'Peso');
INSERT INTO currencies VALUES (8, 'AUD', 'Australian dollar');
INSERT INTO currencies VALUES (9, 'BBD', 'Barbadian dollar');
INSERT INTO currencies VALUES (10, 'BDT', 'Taka');
INSERT INTO currencies VALUES (11, 'BEF', 'Belgian franc');
INSERT INTO currencies VALUES (12, 'BGN', 'Lev');
INSERT INTO currencies VALUES (13, 'BHD', 'Bahraini dinar');
INSERT INTO currencies VALUES (14, 'BMD', 'Bermudian dollar');
INSERT INTO currencies VALUES (15, 'BOB', 'Boliviano');
INSERT INTO currencies VALUES (16, 'BRL', 'Real');
INSERT INTO currencies VALUES (17, 'BSD', 'Bahamian dollar');
INSERT INTO currencies VALUES (18, 'BWP', 'Pula');
INSERT INTO currencies VALUES (19, 'BZD', 'Belize dollar');
INSERT INTO currencies VALUES (20, 'CAD', 'Canadian dollar');
INSERT INTO currencies VALUES (21, 'CDF', 'Congolese franc');
INSERT INTO currencies VALUES (22, 'CHF', 'Swiss franc');
INSERT INTO currencies VALUES (23, 'CLP', 'Peso');
INSERT INTO currencies VALUES (24, 'CNY', 'Chinese yuan');
INSERT INTO currencies VALUES (25, 'COP', 'Peso');
INSERT INTO currencies VALUES (26, 'CRC', 'Costa Rican col�n');
INSERT INTO currencies VALUES (27, 'CUC', 'Cuban convertible peso');
INSERT INTO currencies VALUES (28, 'CUP', 'Cuban peso');
INSERT INTO currencies VALUES (29, 'CZK', 'Czech koruna');
INSERT INTO currencies VALUES (30, 'DKK', 'Danish krone');
INSERT INTO currencies VALUES (31, 'DZD', 'Algerian dinar');
INSERT INTO currencies VALUES (32, 'EEK', 'Estonian kroon');
INSERT INTO currencies VALUES (33, 'EGP', 'Egyptian Pound');
INSERT INTO currencies VALUES (34, 'ESP', 'Spanish peseta');
INSERT INTO currencies VALUES (35, 'ETB', 'Birr');
INSERT INTO currencies VALUES (36, 'EUR', 'Euro');
INSERT INTO currencies VALUES (37, 'FJD', 'Fijan dollar');
INSERT INTO currencies VALUES (38, 'GBP', 'Pound sterling');
INSERT INTO currencies VALUES (39, 'GEL', 'Lari');
INSERT INTO currencies VALUES (40, 'GHS', 'Ghana cedi');
INSERT INTO currencies VALUES (41, 'GMD', 'Dalasi');
INSERT INTO currencies VALUES (42, 'GTQ', 'Quetzal');
INSERT INTO currencies VALUES (43, 'HKD', 'Hong Kong dollar');
INSERT INTO currencies VALUES (44, 'HNL', 'Lempira');
INSERT INTO currencies VALUES (45, 'HRK', 'Kuna');
INSERT INTO currencies VALUES (46, 'HTG', 'Gourde');
INSERT INTO currencies VALUES (47, 'HUF', 'Forint');
INSERT INTO currencies VALUES (48, 'IDR', 'Rupiah');
INSERT INTO currencies VALUES (49, 'ILS', 'Shekel');
INSERT INTO currencies VALUES (50, 'INR', 'Indian Rupee');
INSERT INTO currencies VALUES (51, 'IQD', 'Iraqi dinar');
INSERT INTO currencies VALUES (52, 'IRR', 'Rial');
INSERT INTO currencies VALUES (53, 'ISK', 'Icelandic kr�na');
INSERT INTO currencies VALUES (54, 'ITL', 'Italian lira');
INSERT INTO currencies VALUES (55, 'JMD', 'Jamaican dollar');
INSERT INTO currencies VALUES (56, 'JOD', 'Jordanian dinar');
INSERT INTO currencies VALUES (57, 'JPY', 'Yen');
INSERT INTO currencies VALUES (58, 'KES', 'Kenyan shilling');
INSERT INTO currencies VALUES (59, 'KHR', 'Riel');
INSERT INTO currencies VALUES (60, 'KPW', 'North Korean won');
INSERT INTO currencies VALUES (61, 'KRW', 'South Korean won');
INSERT INTO currencies VALUES (62, 'KWD', 'Kuwaiti dinar');
INSERT INTO currencies VALUES (63, 'KYD', 'Caymand Islands dollar');
INSERT INTO currencies VALUES (64, 'KZT', 'Tenge');
INSERT INTO currencies VALUES (65, 'LAK', 'Kip');
INSERT INTO currencies VALUES (66, 'LBP', 'Lebanese pound');
INSERT INTO currencies VALUES (67, 'LKR', 'Sri Lankan Rupee');
INSERT INTO currencies VALUES (68, 'LRD', 'Liberian dollar');
INSERT INTO currencies VALUES (69, 'LTL', 'Lithuanian litas');
INSERT INTO currencies VALUES (70, 'LVL', 'Lats');
INSERT INTO currencies VALUES (71, 'LYD', 'Dinar');
INSERT INTO currencies VALUES (72, 'MAD', 'Moroccan dirham');
INSERT INTO currencies VALUES (73, 'MCF', 'Monegasque franc');
INSERT INTO currencies VALUES (74, 'MGA', 'Malagasy ariary');
INSERT INTO currencies VALUES (75, 'MKD', 'Macedonian dollar');
INSERT INTO currencies VALUES (76, 'MMK', 'kyat');
INSERT INTO currencies VALUES (77, 'MNT', 'T�gr�g');
INSERT INTO currencies VALUES (78, 'MTL', 'Maltese lira');
INSERT INTO currencies VALUES (79, 'MVR', 'Maldivian Rufiyaa');
INSERT INTO currencies VALUES (80, 'MWK', 'Kwacha');
INSERT INTO currencies VALUES (81, 'MXN', 'Peso');
INSERT INTO currencies VALUES (82, 'MYR', 'Ringgit');
INSERT INTO currencies VALUES (83, 'MZN', 'Mozambican metical');
INSERT INTO currencies VALUES (84, 'NAD', 'Namibian dollar');
INSERT INTO currencies VALUES (85, 'NDK', 'Norwegian krone');
INSERT INTO currencies VALUES (86, 'NGN', 'Naira');
INSERT INTO currencies VALUES (87, 'NIO', 'C�rdoba');
INSERT INTO currencies VALUES (88, 'NLG', 'Dutch guilder');
INSERT INTO currencies VALUES (89, 'NPR', 'Nepalese rupee');
INSERT INTO currencies VALUES (90, 'NZD', 'New Zealand dollar');
INSERT INTO currencies VALUES (91, 'OMR', 'Rial');
INSERT INTO currencies VALUES (92, 'PAB', 'Balboa');
INSERT INTO currencies VALUES (93, 'PEN', 'Nuevo Sol');
INSERT INTO currencies VALUES (94, 'PHP', 'Peso');
INSERT INTO currencies VALUES (95, 'PKR', 'Pakistani Rupee');
INSERT INTO currencies VALUES (96, 'PLN', 'Zloty');
INSERT INTO currencies VALUES (97, 'QAR', 'Riyal');
INSERT INTO currencies VALUES (98, 'RON', 'Romanian leu');
INSERT INTO currencies VALUES (99, 'RUB', 'Ruble');
INSERT INTO currencies VALUES (100, 'RWF', 'Rwandan franc');
INSERT INTO currencies VALUES (101, 'SAR', 'Saudi riyal');
INSERT INTO currencies VALUES (102, 'SDG', 'Sudanese pound');
INSERT INTO currencies VALUES (103, 'SEK', 'Swedish krona');
INSERT INTO currencies VALUES (104, 'SGD', 'Singapore dollar');
INSERT INTO currencies VALUES (105, 'SYP', 'Syrian pound');
INSERT INTO currencies VALUES (106, 'SZL', 'Lilangeni');
INSERT INTO currencies VALUES (107, 'THB', 'Baht');
INSERT INTO currencies VALUES (108, 'TND', 'Tunisian dinar');
INSERT INTO currencies VALUES (109, 'TOP', 'Pa?anga');
INSERT INTO currencies VALUES (110, 'TRY', 'Turkish lira');
INSERT INTO currencies VALUES (111, 'TTD', 'Trinidad and Tobago dollar');
INSERT INTO currencies VALUES (112, 'TWD', 'New Taiwan dollar');
INSERT INTO currencies VALUES (113, 'TZS', 'Tanzanian shilling');
INSERT INTO currencies VALUES (114, 'UAH', 'Hryvnia');
INSERT INTO currencies VALUES (115, 'UGX', 'Ugandan shilling');
INSERT INTO currencies VALUES (116, 'USD', 'United States dollar');
INSERT INTO currencies VALUES (117, 'UYU', 'Uruguayan peso');
INSERT INTO currencies VALUES (118, 'UZS', 'Uzbekistan som');
INSERT INTO currencies VALUES (119, 'VEF', 'Bolivar fuerte');
INSERT INTO currencies VALUES (120, 'VND', '?ong');
INSERT INTO currencies VALUES (121, 'WST', 'Tala');
INSERT INTO currencies VALUES (122, 'XAF', 'Central African CFA franc');
INSERT INTO currencies VALUES (123, 'XOF', 'West African CFA franc');
INSERT INTO currencies VALUES (124, 'XPF', 'CFP franc');
INSERT INTO currencies VALUES (125, 'YER', 'Yemeni rial');
INSERT INTO currencies VALUES (126, 'ZAR', 'Rand');
INSERT INTO currencies VALUES (127, 'ZMK', 'Zambian kwacha');
INSERT INTO currencies VALUES (128, 'LRUSD', 'Liberty Reserve United States dollar');
INSERT INTO currencies VALUES (130, 'LREUR', 'Liberty Reserve Euros');
INSERT INTO currencies VALUES (131, 'PPUSD', 'PayPal US dollar');
INSERT INTO currencies VALUES (132, 'PPEUR', 'PayPal Euro');
INSERT INTO currencies VALUES (133, 'DWUSD', 'Dwolla US dollar');
INSERT INTO currencies VALUES (134, 'MBUSD', 'Moneybookers US dollar');
INSERT INTO currencies VALUES (135, 'MBEUR', 'Moneybookers Euro');
INSERT INTO currencies VALUES (136, 'BMUSD', 'Bitcoin Market US dollar');
INSERT INTO currencies VALUES (137, 'MLUSD', 'Mail US dollar');
INSERT INTO currencies VALUES (129, 'PXGAU', 'Grams of gold');
INSERT INTO currencies VALUES (138, 'BMAUD', 'Bitcon Market Australian dollar');
INSERT INTO currencies VALUES (139, 'SLL', 'Second Life Linden dollars');
INSERT INTO currencies VALUES (140, 'SLC', 'SolidCoin');
INSERT INTO currencies VALUES (141, 'IXC', 'Ixcoin');
INSERT INTO currencies VALUES (142, 'I0C', 'I0coin');
INSERT INTO currencies VALUES (143, 'NMC', 'Namecoin');


--
-- Data for Name: exchanges; Type: TABLE DATA; Schema: public; Owner: solidcoinmarkets
--

INSERT INTO exchanges VALUES (1, 'solidcoin24', 'https://solidcoin24.com/', 'admin@solidcoin24.com', '', '', '', 'solidcoin24');


--
-- Data for Name: markets; Type: TABLE DATA; Schema: public; Owner: solidcoinmarkets
--

INSERT INTO markets VALUES (1, 'sldcn24BTC', 'https://solidcoin24.com/', 'https://solidcoin24.com/api/ticker/btc', 'https://solidcoin24.com/api/offers/btc/orderbook', 'https://solidcoin24.com/api/trades/btc/n/2000', 1, '', '', true, 1, '0.1%', 'exchange', NULL);
INSERT INTO markets VALUES (2, 'sldcn24EUR', 'https://solidcoin24.com/', 'https://solidcoin24.com/api/ticker/eur', 'https://solidcoin24.com/api/offers/eur/orderbook', 'https://solidcoin24.com/api/trades/eur/n/2000', 36, '', '', true, 1, '0.1%', 'exchange', '');
INSERT INTO markets VALUES (3, 'sldcn24USD', 'https://solidcoin24.com/', 'https://solidcoin24.com/api/ticker/usd', 'https://solidcoin24.com/api/offers/usd/orderbook', 'https://solidcoin24.com/api/trades/usd/n/2000', 116, '', '', true, 1, '0.1%', 'exchange', '');
INSERT INTO markets VALUES (4, 'sldcn24NMC', 'https://solidcoin24.com/', 'https://solidcoin24.com/api/ticker/nmc', 'https://solidcoin24.com/api/offers/nmc/orderbook', 'https://solidcoin24.com/api/trades/nmc/n/2000', 143, '', '', true, 1, '0.1%', 'exchange', '');


--
-- Name: currencies_currency_abbreviation_unique; Type: CONSTRAINT; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_currency_abbreviation_unique UNIQUE (currency_abbreviation);


--
-- Name: currencies_pkey; Type: CONSTRAINT; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_pkey PRIMARY KEY (currency_id);


--
-- Name: depth_pkey; Type: CONSTRAINT; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

ALTER TABLE ONLY depth
    ADD CONSTRAINT depth_pkey PRIMARY KEY (depth_id);


--
-- Name: exchanges_exchange_devname_key; Type: CONSTRAINT; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

ALTER TABLE ONLY exchanges
    ADD CONSTRAINT exchanges_exchange_devname_key UNIQUE (exchange_devname);


--
-- Name: exchanges_pkey; Type: CONSTRAINT; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

ALTER TABLE ONLY exchanges
    ADD CONSTRAINT exchanges_pkey PRIMARY KEY (exchange_id);


--
-- Name: markets_pkey; Type: CONSTRAINT; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

ALTER TABLE ONLY markets
    ADD CONSTRAINT markets_pkey PRIMARY KEY (market_id);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (settings_var);


--
-- Name: depth_depth_market_id_depth_timestamp_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE UNIQUE INDEX depth_depth_market_id_depth_timestamp_idx ON depth USING btree (depth_market_id, depth_timestamp);


--
-- Name: depth_depth_timestamp_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE INDEX depth_depth_timestamp_idx ON depth USING btree (depth_timestamp);


--
-- Name: markets_market_currency_id_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE INDEX markets_market_currency_id_idx ON markets USING btree (market_currency_id);


--
-- Name: tickers_ticker_timerstamp; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE INDEX tickers_ticker_timerstamp ON tickers USING btree (ticker_timestamp);


--
-- Name: trade_volumes_market_id_day_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE UNIQUE INDEX trade_volumes_market_id_day_idx ON trade_volumes USING btree (trade_volume_market_id, trade_volume_day);


--
-- Name: trades_trade_date_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE INDEX trades_trade_date_idx ON trades USING btree (trade_date);


--
-- Name: trades_trade_exchange_id_trade_date_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE INDEX trades_trade_exchange_id_trade_date_idx ON trades USING btree (trade_market_id, trade_date);


--
-- Name: trades_trade_market_id_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE INDEX trades_trade_market_id_idx ON trades USING btree (trade_market_id);


--
-- Name: trades_trade_market_id_trade_date_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE INDEX trades_trade_market_id_trade_date_idx ON trades USING btree (trade_market_id, trade_date);


--
-- Name: trades_trade_market_id_trade_id_idx; Type: INDEX; Schema: public; Owner: solidcoinmarkets; Tablespace: 
--

CREATE UNIQUE INDEX trades_trade_market_id_trade_id_idx ON trades USING btree (trade_market_id, trade_id);


--
-- Name: markets_exchange_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: solidcoinmarkets
--

ALTER TABLE ONLY markets
    ADD CONSTRAINT markets_exchange_id_fkey FOREIGN KEY (market_exchange_id) REFERENCES exchanges(exchange_id);


--
-- Name: markets_market_currency_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: solidcoinmarkets
--

ALTER TABLE ONLY markets
    ADD CONSTRAINT markets_market_currency_id_fkey FOREIGN KEY (market_currency_id) REFERENCES currencies(currency_id);


--
-- Name: tickers_ticker_market_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: solidcoinmarkets
--

ALTER TABLE ONLY tickers
    ADD CONSTRAINT tickers_ticker_market_id_fkey FOREIGN KEY (ticker_market_id) REFERENCES markets(market_id);


--
-- Name: trade_volumes_market_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: solidcoinmarkets
--

ALTER TABLE ONLY trade_volumes
    ADD CONSTRAINT trade_volumes_market_id_fkey FOREIGN KEY (trade_volume_market_id) REFERENCES markets(market_id);


--
-- Name: trades_trade_market_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: solidcoinmarkets
--

ALTER TABLE ONLY trades
    ADD CONSTRAINT trades_trade_market_id_fkey FOREIGN KEY (trade_market_id) REFERENCES markets(market_id);


--
-- PostgreSQL database dump complete
--

